
var idbox = $('aside p');

$('.choice').click(function(){
  var val = $(this).attr('data-value');
  var box = $(this).parent('.choicelist').parent('.image-box');
  var img = box.children('img');
  if (val === "yes") {
    addimage(img);
  }else {
    rmimage(img);
  }
});

function addimage(img){

  var box = img.parent('.image-box');
  var id = box.attr('id');
  var idlist = $('#ids').val();
  if (idlist != "") {
    var newid = idlist+', '+id;
  }else {
    var newid = id;
  }
  box.addClass('added');
  img.clone().prependTo('.selected-images');
  $('#ids').attr('value', newid);
}
function rmimage(img){
  var box = img.parent('.image-box');
  var img = box.children('img');
  box.addClass('removed');
}

    var theInput = document.getElementById("colorinput");
    var theColor = theInput.value;
    theInput.addEventListener("input", function() {

    $("#hex").css('backgroundColor',theInput.value);
    }, false);

$('.helpbutton').click(function(){
  $('body').toggleClass('help-opened');
})

// Multiple form actions
function submitForm(outputpage){
  document.getElementById('outputs').action = outputpage;
  document.getElementById('outputs').submit();
}
