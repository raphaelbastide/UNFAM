<?php
include "header.php";
require_once 'functions.php';
$url = $_POST["url"];
$attribution = $_POST["attribution"];
$extarray = array("gif","png" ,"jpg","svg", "JPG");
if((!empty($url))) {
//Сheck that we have a file
  $info = new SplFileInfo($url);
  $ext = $info->getExtension();
  $name = $info->getBasename($ext);
  $slug = slugify($name);
  $newname = $slug.".".$ext;
  $licensepath = "image-base/".$slug.".txt";
  // $ext = substr($id, strrpos($id, '.') + 1);
  if ((in_array($ext,$extarray))) {
      $newpath = dirname(__FILE__).'/image-base/'.$newname;
      if (!file_exists($newpath) && !file_exists($licensepath)) {
        //Attempt to move the uploaded file to it's new place

        $ch = curl_init($url);
        $fp = fopen($newpath, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        // Write license
        $licensefile = fopen($licensepath, "w") or die("Unable to open file!");
        fwrite($licensefile, $attribution);
        fclose($licensefile);

        $notify = "succes";

      } else {
        $notify = "exist";
      }
  } else {
        $notify = "ext";
  }
} else {
  $notify = "missing";
}
// echo "<p><a href='/'>home</a> or <a href='add.php'>add something else</a></p>";
header('Location: /?'.$notify);
include "footer.php";
?>
