<span class="helpbutton button">?</span>
<div class="help">
  <p>This is UNFAM, an experimental web app to make stuff (documents, objects, web pages…) with unfamous and unfamiliar graphic content.</p>
  <p>You can interact with this app by three different approaches, here is how:</p>
  <ul>
    <li>GENERATE: Use the YES / NO choice select the images you want, and click on one of the buttons the right sidebar to chose your output.</li>
    <li>FEED: Pick strange, unfamous, weird images and other graphic material on the last search result pages and dark corners of the web. Be sure to cast only material under <a href="http://creativecommons.org/">Creative Commons</a> or open source licenses! Once you have chosen an image, use the form on the top of this page and paste its URL and its author name or nickname.</li>
    <li>HACK: Create a now output by copying one of the existing <code>php</code> file in the <code>outputs/</code> folder, rename it and change its name line 2. Your output will be listed among the existing output buttons on the right sidebar. May require HTML / CSS and / or PHP basics.</li>
  </ul>

  <p class="">A project by Raphaël Bastide made for a masterclass at the Parsons School of Paris. This project is under an open source license (GPL). The source can be downloaded <a href="https://gitlab.com/raphaelbastide/UNFAM">here</a>.</p>

  <div class="tutorial">
    <p class="tutmedia">Output list</p>
    <p class="tutadd">Add a new image</p>
    <p class="tutyesno">Select the images you want</p>
  </div>
</div>
