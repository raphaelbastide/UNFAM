<?php
// Demo
//
include_once("../outputHeader.php");
$color = urldecode($_GET["color"]);
$selection = urldecode($_GET["ids"]);
$selection = explode(', ', $selection);
?>
<!-- From here begins the HTML page. Do not modify the PHP lines above (Exept the word “Demo”) to ensure the images selection will be collected -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Demo</title>
    <style>
      /* Here is the style part (CSS). */
      img{
        height:400px; width: auto;
      }
    </style>
  </head>
  <!-- The line below will add a inline style to the page’s body to apply the background color chosed by the user -->
  <body style="background:<?php echo $color ?>;">
    <p>This is a demo output. You can do whatever you want with the following image. Web page, animation, poster, book, banner…</p>
    <p>To do so, copy one of the existing <code>php</code> file in the <code>outputs/</code> folder, rename it and change its name line 2.</p>
    <?php
    $selection = array_filter($selection);
    // Check if the Selection is empty or not
    if (!empty($selection)) {
      // If images selection is not empty, list images one after the other
      foreach ($selection as $image) {
        // Here we create image tags according to the selection passed through the URL
        echo "<img src='../image-base/$image'>";
      }
    }else {
      // If no selection, display this message
      echo "<em>Nothing selected! Please select some images.</em>";
    }
    ?>
  </body>
</html>
