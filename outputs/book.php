<?php
// Book
//
include_once("../outputHeader.php");
$color = urldecode($_GET["color"]);
$selection = urldecode($_GET["ids"]);
$selection = explode(', ', $selection);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Book</title>
    <style>
      body{
        font-family: monospace;
        padding: 100px;
      }
      section img{
        display: block;
        margin: 0 auto;
        height:400px; width: auto;
      }
      textarea, input{
        width:100%;
        border:1px solid #ccc;
        padding:5px;
      }
      .btnprint{
        color:inherit;
        background:#fff;
        border:1px solid #ccc;
        position: fixed;
        top:5px;
        left:5px;
        padding: 10px;
        border-radius: 10px;
        text-decoration: none;
      }
      /* Print specific style */
      @media print {
        html,body{height:100%;}
        body{
          margin:0;
          padding:0;
        }
        .btnprint{
          display: none;
        }
        @page{
          margin: 0;
        }
        body>header{
          padding-left:.7cm;
        }
        .title{
          margin-top:40vh;
          font-size: 6vh;
        }
        .author{
          margin-top:10vh;
          font-size: 2vh;
          display: inl
        }
        .captions{
          font-size:10vh;
        }
        section{
          page-break-before: always;
          position: relative;
          padding-top:10vh;
        }
        section img{
          display: block;
          margin: 0 auto;
          height:400px; width: auto;
        }

        textarea, input{
          text-align:center;
          background: none;
          border:none;
          text-align: center;
        }
      }
    </style>
  </head>
  <body style="background:<?php echo $color ?>;">
    <a href="#" class="btnprint button" onclick="window.print();">Print</a>
    <input class="title" type="text" name="" value="Your title here">
    <input class="author" type="text" name="" value="Author">
    <?php
      $selection = array_filter($selection);
      if (!empty($selection)) {
        foreach ($selection as $image) {
          echo "<section>";
          echo "<img src='../image-base/$image'>";
          echo "<textarea></textarea>";
          echo "</section>";
        }
      }else {
        echo "<em>Nothing selected! Please select some images.</em>";
      }
    ?>
  </body>
</html>
