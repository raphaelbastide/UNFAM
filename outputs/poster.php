<?php
// Poster
//
include_once("../outputHeader.php");
$color = urldecode($_GET["color"]);
$selection = urldecode($_GET["ids"]);
$selection = explode(', ', $selection);
?>

<!-- Modify this ouptput title line 2 and keep the lines above -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8 />
    <title>Overlap</title>
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" media="all" href="https://fontlibrary.org/face/lsw-drachenklaue" type="text/css"/>
    <link rel="stylesheet" media="all" href="https://fontlibrary.org/face/c-reduci" type="text/css"/>
    <style>
    html,body{height:100%; background: white;}
    main, .postershadow{
      /*font-family: LSWDrachenklaueDrachenklaueSamys01;*/
      width: 29.7cm;
      height:43.2cm;
      text-align: center;
      overflow: hidden;
      transition: all .4s ease-in;
    }
    @page{
      size: 29.7cm 43.2cm;
      margin: 0cm;
    }
    main{
      position: relative;
      overflow: hidden;
    }
    main.transformed, .postershadow{
      transform:scale3d(0.4, 0.4, 1) rotate3d(12, 9, 6, -30deg);
      top:-50vh;
    }
    .postershadow{
      display: block;
      box-shadow: -30px 40px 80px #888;
      position: absolute;
      z-index: 0;
    }
    img.main{
      max-width: 300px;
      display: inline-block;
      height: auto;
    }
    img.small{
      max-width:100px;
      height: auto;
    }
    img.round{
      border-radius: 20%;
    }
    img.trapeze{
      transform: scale(1) rotate(360deg) translate(20px, 0px) skew(320deg, 151deg);
    }
    img.shadow{
      box-shadow: 0 30px 70px #fff;
    }
    img.tile{
      width:100%;
      height:100%;
      background: url()
    }

    .attribution-list{
      position: absolute;;
      width:100%;
      bottom:0;
      left:0;
    }
    section.attribution{
      font-family: creduciMedium;
      /*box-shadow: inset 0 0 10px #ccc;*/
      padding:10px;
      float: left;
      width:50%;
      height: auto;
      font-size: 30px;
      background: white;
      text-align: left;
      vertical-align: middle;
    }
    section.attribution img{
      display: inline-block;
      height: 50px;
      width: auto;
      margin-right: 30px;
      vertical-align: middle;
    }
    .random{
      position:absolute;
      font-size: 30vw;
      color:white;
      width:400px;
      text-shadow: 0 39px 10px #ccc;
    }

    @media print {
      main.transformed{
        transform:none;
        overflow: hidden;
        top:0vh;
      }
    }
    </style>
  </head>
  <body >
    <main class="transformed" style="background:<?php echo $color ?>;">

    <?php
    $image_number = 0;
    // List images and attributions
    foreach ($selection as $image) {
      $info = new SplFileInfo($image);
      $ext = $info->getExtension();
      $name = $info->getBasename(".".$ext);
      if (file_exists("../image-base/$name.txt")) {
        $attribution = file_get_contents("../image-base/$name.txt");
        // echo "<span class='random'>$attribution</span>";
      }
      $image_number ++;
    }

    $image_size = 100 / $image_number;
    $classes = ['small', 'trapeze', 'round', 'trapeze', 'shadow' , 'tile' , 'none', 'none' , 'none'];
    // List all images
    foreach ($selection as $image) {
      $classlist = $classes[array_rand($classes)];
      echo "<img class='random $classlist' src='../image-base/$image'>";
    }
    ?>

      <div class="attribution-list">
        <?php
        // List images and attributions
        foreach ($selection as $image) {
          $info = new SplFileInfo($image);
          $ext = $info->getExtension();
          $name = $info->getBasename(".".$ext);
          if (file_exists("../image-base/$name.txt")) {
            echo "<section class='attribution'>";
            echo "<img src='../image-base/$image'>";
            $attribution = file_get_contents("../image-base/$name.txt");
            echo "<span>$attribution</span>";
            echo "</section>";
          }
        }
        ?>
      </div>
    </main>
    <div class="postershadow"></div>
  </body>
  <script src="../js/cola.min.js" charset="utf-8"></script>
  <script src="../js/rectangletest.js" charset="utf-8"></script>
  <script type="text/javascript">
    var img = document.querySelectorAll('.random');
    for (var i = 0; i < img.length; i++) {
      var randx = Math.random() * (80) - 10;
      var randy = Math.random() * (80) - 10;
      img[i].style.left = randx+"%";
      img[i].style.top = randy+"%";
    }
  </script>
</html>
