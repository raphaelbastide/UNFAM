<?php
// closeup
//
include_once("../outputHeader.php");
$color = urldecode($_GET["color"]);
$selection = urldecode($_GET["ids"]);
$selection = explode(', ', $selection);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Closeup</title>
    <style>

    .slideshow-container{
      position:absolute;
      animation: a 30s infinite;
      top:0;
      left:0;
      width:100%;
    }
    .slideshow-container.horizontal .cycle-slideshow img{
      height:100vh;
      width:auto;
    }
    .cycle-slideshow img{
      width:100%;
      height:auto;
    }
    </style>
  </head>
  <body style="background:<?php echo $color ?>;">
    <div class="slideshow-container">
      <div class="cycle-slideshow"
          data-cycle-speed="9000"
          data-cycle-manual-speed="300"
          data-cycle-timeout="1000">
      <?php
      foreach ($selection as $image) {
        echo "<img src='../image-base/$image'>";
      }
      ?>
      </div>
    </div>

    <!-- Javascript for the image slideshow -->
    <script src="../js/jquery-3.1.0.min.js" charset="utf-8"></script>
    <script src="../js/cycle2.min.js" charset="utf-8"></script>
    <script src="../js/keyframes.min.js" charset="utf-8"></script>
    <script type="text/javascript">
      $(document.documentElement).keyup(function (event) {
      if (event.keyCode == 37) {
        $('.cycle-slideshow').cycle('prev');
      } else if (event.keyCode == 39) {
        console.log('te')
        $('.cycle-slideshow').cycle('next');
      }
    });


    var container = $('.slideshow-container');
    window.onresize = function(event) {
      var winH = $(window).height(),
          winW = $(window).width(),
          winR = winW / winH,
          imgH = $('.cycle-slideshow').height(),
          imgW = $('.cycle-slideshow img').width(),
          diffH = Math.round(winW - imgW);
          diffV = Math.round(winH - imgH);
      if (winR <= 1.5) {
        container.addClass('horizontal');
        $.keyframe.define([{
          name: 'a',
          '0%': {'left': '0px'},
          '50%': {'left': diffH+"px"},
          '100%': {'left': '0px'}
        }]);
      }else {
        container.removeClass('horizontal');
        $.keyframe.define([{
          name: 'a',
          '0%': {'top': '0px'},
          '50%': {'top': diffV+"px"},
          '100%': {'top': '0px'}
        }]);
      }
    };
    setTimeout(function(){$(window).resize();},1000);
    </script>
  </body>
</html>
