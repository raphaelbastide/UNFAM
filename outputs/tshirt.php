<?php
// Tshirt
//
include_once("../outputHeader.php");
$color = urldecode($_GET["color"]);
$selection = urldecode($_GET["ids"]);
$selection = explode(', ', $selection);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Tshirt</title>
    <style>
    body{
      text-align: center;
      padding-top:10vh;
      background-image: radial-gradient(ellipse farthest-corner at 50% 50% , #ccc 0%, #333 95%);
    }
      img{max-height:200px; width: auto;}
      .background{
        margin:0 auto;
        clip-path: url(#clip);
        text-align: center;
        height:100vh;
        width:600px;
      }
      .background img{
        display: inline-block;
      }
    </style>
  </head>
  <body>
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg  height="0" width="0">

  <clipPath id="clip">

  <path
     id="path3721"
     d="M 0,66.235125 176.62703,2.9109729e-5 h 22.07838 c 0,0 20.94356,34.541183890271 44.15674,44.156763890271 23.21323,9.615563 65.10033,9.615563 88.31353,0 C 354.3889,34.541213 375.3324,2.9109729e-5 375.3324,2.9109729e-5 L 397.4108,0 574.03787,66.235125 551.95951,176.62709 h -88.3136 V 485.72411 H 110.39189 V 176.62709 H 22.078386 Z"

   </clipPath>
</svg>

  <div class="background" style="background:<?php echo $color ?>;">

    <?php
    shuffle($selection);
    foreach ($selection as $image) {
      echo "<img src='../image-base/$image'>";
    }
    ?>
  </div>
  </body>
</html>
