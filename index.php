<?php
include "header.php";
require_once 'functions.php';
?>
  <aside id="hex" class="selection">
    <form id="outputs" action="" method="get">
      <input id="colorinput" type="color" name="color" value="#cccccc">
      <div class="selected-images"></div>
      <input id="ids" hidden type="text" name="ids" value="">
      <div class="buttons">

      <?php
      $output_dir = "outputs/";
      $outputs = glob($output_dir."{*.php}", GLOB_BRACE);
      // sort($images);
      foreach ($outputs as $output) {
        $id = basename($output);
        $info = new SplFileInfo($output);
        $content = file_get_contents($output);

        if ($content != "") {
          $title = get_string_between($content, "// ", "//");
           ?>
           <input class="button generate" type="submit" onclick="submitForm('<?php echo $output ?>')" value="<?php echo $title ?>" />
           <?php
        }else {
          echo "<p>Output is undefined<p>";
        }
      }
      ?>
      </div>
    </form>
  </aside>
  <main>
    <form class="add" action="upload.php" method="post">
      <input type="text" required name="url" class="input" placeholder="Creative Commons Image URL">
      <input type="text" required name="attribution" class="input" placeholder="Author attribution">
      <input type="submit" value="Add a new image" class="button">
    </form>
    <?php
    $project_dir = "image-base/";
    $images = glob($project_dir."{*.jpg,*JPEG,*.JPG,*.jpeg,*.gif,*.png,*.svg}", GLOB_BRACE);
    echo "<section class='image'>";
    shuffle($images);

    // sort($images);
    foreach ($images as $img) {
      list($width, $height, $type, $attribution) = getimagesize($img);
      $id = basename($img);
      $info = new SplFileInfo($img);
      $ext = $info->getExtension();
      $name = $info->getBasename($ext);
      $slug = slugify($name);

    ?>
      <div class="image-box" id="<?=$id?>">
      <img class='image' src='<?=$img?>' width='<?=$width?>' height='<?=$height?>' />
      <p class="attribution"><?php
      if (file_exists($project_dir.$slug) && $attribution != "") {
        $attribution = file_get_contents($project_dir.$slug.".txt");
        echo $attribution;
      }else {
        echo "Undefined";
      }
      ?></p>
      <div class="choicelist">
        <p class="choice yes" data-value="yes" id='yes-<?=$id?>'><span>yes</span></p>
        <p class="choice no" data-value="no" id='n data=""-<?=$id?>'><span>no</span></p>
      </div>
    </div>
  <?php
    }
    echo "</section>";
  ?>
  </main>
<?php
  include "help.php";
  include "footer.php";
?>
